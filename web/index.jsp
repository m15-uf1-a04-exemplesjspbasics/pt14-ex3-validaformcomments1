<%-- 
    Document   : VALIDACIÓ FORMULARI COMENTARIS
    Author     : DAWBIO - PROFE
--%>
<%@page import="ValidaFormComments.ValidadorFormularis"%>
<%@page import="ValidaFormComments.*" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- <style rel="stylesheet" >
            body {background-color: powderblue;}
            h2   {color: red;}
            p    {color: green;}
        </style> -->
        <link rel="stylesheet" href="./css/styles.css"> 
        <title>M15-UF1-PT4-JSP-EX3-FORMPAG1</title>
    </head>
    <body>
        <h2>VALIDACIÓ FORMULARI COMENTARIS</h2>
        <form method="post" action="">
            <p><label>Nom (*)</label>
                <input type="text" name="nom"></p>
            <p><label>Email (*)</label>
                <input type="text" name="email"></p>
            <p><label>Comentari (*)</label> <br>
<!--         <select name="conversio">
                <option value="adn-arn">ADN-ARN</option>
                <option value="arn-adn">ARN-ADN</option>
             </select>-->
             <textarea name="comment" rows="10" cols="30" 
                   minlength="10" maxlength="140"></textarea>
             </p>
             <input type="checkbox" name="spamok" value="true">(*) Acepto recibir spam <BR>
            <input type="submit" name="btnEnviar" value="Enviar"/>
        </form>  
        
        <%
           boolean campsInformats = false;
           boolean campsValids = false;
           
           if(request.getParameter("btnEnviar")!=null) {
              // 1. VALIDACIO CAMPS NO BUITS.
             String nom = request.getParameter("nom");
             String comment = request.getParameter("comment");
             String spamok = request.getParameter("spamok");
             String email = request.getParameter("email");
           
              ValidadorFormularis validador = new ValidadorFormularis(); 
              campsInformats = 
                 validador.validaCampNoBuit(nom) && 
                     validador.validaCampNoBuit(comment) &&
                        validador.validaCampNoBuit(email);
              
              campsInformats = campsInformats && spamok!=null
                      && spamok.equals("true");
              // out.println("campsInformats="+campsInformats);
              
              if (campsInformats) {
                  boolean errorComment = 
                          validador.validaNoParaulesOfensives(comment);
                  boolean errorEmail = validador.validaCampEmail(email);
                  
                  // 2. VALIDACIO CAMPS CORRECTES.
                  campsValids = !errorComment && !errorEmail;
                      
                  out.println(campsValids);
                  // Si els camps son valids anem a la altra JSP
                  if(campsValids) {
                    request.getRequestDispatcher("formvalid.jsp").forward(request, response);
                    
                  } else {
                      // Mostrar camps que no son valids i el motiu.
                       out.println("Els següents camps són incorrectes: Email o Comentari.");
                  }
                  
              } else {
                // Si no es vàlid, retorna missatge.
                out.println("T'ha faltat omplir camps. Alguns estaven buits.");
              }
              // out.println("el comentari no és vàlid");
              
           } else {
                // Si no es vàlid, retorna missatge.
                out.println("Error al enviar el formulari.");
           }
           
        %>
        
    </body>
</html>
